get '/' do
  # let user create new short URL, display a list of shortened URLs
  erb :index
end

post '/urls' do
  @url = Url.new(long_url: params[:long_url])
  @url.save

  erb :index
  # create a new Url
end

# e.g., /q6bda
get '/:short_url' do
  # redirect to appropriate "long" URL
  long_urls = Url.where(short_url: params[:short_url]).first
  long_urls.counter += 1
  long_urls.save
  redirect to long_urls.long_url
end