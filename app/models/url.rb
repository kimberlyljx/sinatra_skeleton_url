class Url < ActiveRecord::Base
before_create :generate_short_link

validates_url_format_of :long_url,
                        :allow_nil => true,
                        :message => 'is completely unacceptable'

  def generate_short_link
    @short_link = (0...8).map { (65 + rand(26)).chr }.join
    self.short_url = @short_link
  end

end
